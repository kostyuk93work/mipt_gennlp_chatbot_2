import torch
from transformers import AutoModelWithLMHead, AutoTokenizer
import gradio as gr

from transformers.utils import logging
logging.set_verbosity_error()

device = "cpu"
model_path = 'model/model_epoch_6.pt'

model = AutoModelWithLMHead.from_pretrained("microsoft/DialoGPT-small")
tokenizer = AutoTokenizer.from_pretrained("microsoft/DialoGPT-small")

# Загрузка весов модели
# Загрузка модели на CPU
model.load_state_dict(torch.load(model_path, map_location=torch.device(device)))
# model.load_state_dict(torch.load(model_path))

model.to(device)  # Переместите модель на нужное устройство, например, на GPU, если она доступна



# # Let's chat for 6 lines
# for step in range(6):
#     # encode the new user input, add the eos_token and return a tensor in Pytorch
#     new_user_input_ids = tokenizer.encode(input("User:") + tokenizer.eos_token, return_tensors='pt').to(device)
#     # print(new_user_input_ids)

#     # append the new user input tokens to the chat history
#     bot_input_ids = torch.cat([chat_history_ids, new_user_input_ids], dim=-1) if step > 0 else new_user_input_ids

#     # generated a response while limiting the total chat history to 1000 tokens,
#     chat_history_ids = model.generate(
#         bot_input_ids, max_length=10000,
#         pad_token_id=tokenizer.eos_token_id,
#         no_repeat_ngram_size=3,
#         do_sample=True,
#         top_k=100,
#         top_p=0.7,
#         temperature=0.8
#     )

#     # pretty print last ouput tokens from bot
#     print("Peter: {}".format(tokenizer.decode(chat_history_ids[:, bot_input_ids.shape[-1]:][0], skip_special_tokens=True)))


def chat_bot(input_message, history):
    #history - лист листов [['вопрос1', 'ответ1'],['вопрос2', 'ответ2']...]

    bot_input_ids = None

    #Ограничиваем контекст 6 фразами (выбираем 3 последние парвы вопрос-ответ)
    for message_pair in history[:-4:-1][::-1]:
        for single_message in message_pair:
            message_encoding = tokenizer.encode(single_message + tokenizer.eos_token, return_tensors='pt').to(device)
            if bot_input_ids is not None:
                bot_input_ids = torch.cat([bot_input_ids, message_encoding], dim=-1)
            else:
                bot_input_ids = message_encoding


    new_user_input_ids = tokenizer.encode(input_message + tokenizer.eos_token, return_tensors='pt').to(device)

    if bot_input_ids is not None:
        bot_input_ids = torch.cat([bot_input_ids, new_user_input_ids], dim=-1)
    else:
        bot_input_ids = new_user_input_ids


    # generated a response while limiting the total chat history to 10000 tokens,
    chat_history_ids = model.generate(
        bot_input_ids, max_length=10000,
        pad_token_id=tokenizer.eos_token_id,
        no_repeat_ngram_size=3,
        do_sample=True,
        top_k=100,
        top_p=0.7,
        temperature=0.8
    )
 
    selected_response = tokenizer.decode(chat_history_ids[:, bot_input_ids.shape[-1]:][0], skip_special_tokens=True)
    return selected_response

demo = gr.ChatInterface(
    chat_bot,
    chatbot=gr.Chatbot(height=700),
    textbox=gr.Textbox(placeholder="What are you looking at? Type!", container=False, scale=7),
    title="Peter Griffin",
    description="Hey Lois, I am a chat bot",
    theme="soft",
    examples=["Hello", "Well then, my goal becomes clear", "Damn you, woman, awake from your damnable reverie!"]
    
)

demo.launch(server_name="0.0.0.0", server_port=9010, share=True)
# demo.launch(server_name="localhost", server_port=7010, share=True)
